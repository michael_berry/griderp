Template.SetupWebsiteContactUsSettings.rendered = function() {
	
};

Template.SetupWebsiteContactUsSettings.events({
	
});

Template.SetupWebsiteContactUsSettings.helpers({
	
});

Template.ContactUsSettingsForm.rendered = function() {

	$('.summernote').summernote({
		height: 300,
		focus: true,
		codemirror: {
			htmlMode: true,
			lineNumbers: true,
			mode: 'text/html'
		}
	});

};

Template.ContactUsSettingsForm.events({

});

Template.ContactUsSettingsForm.helpers({

});

