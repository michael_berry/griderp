Template.SetupWebsiteWebsiteSettings.rendered = function() {
	
};

Template.SetupWebsiteWebsiteSettings.events({
	
});

Template.SetupWebsiteWebsiteSettings.helpers({
	
});

Template.WebsiteSettingsForm.rendered = function() {

	$('.summernote').summernote({
		height: 225,
		focus: true,
		codemirror: {
			htmlMode: true,
			lineNumbers: true,
			mode: 'text/html'
		}
	});

};

Template.WebsiteSettingsForm.events({

});

Template.WebsiteSettingsForm.helpers({

});

